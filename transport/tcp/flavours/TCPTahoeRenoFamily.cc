//
// Copyright (C) 2004 Andras Varga
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include "TCPTahoeRenoFamily.h"
#include "TCP.h"



TCPTahoeRenoFamilyStateVariables::TCPTahoeRenoFamilyStateVariables()
{
    ssthresh = 65535;
}

std::string TCPTahoeRenoFamilyStateVariables::info() const
{
    std::stringstream out;
    out << TCPBaseAlgStateVariables::info();
    out << " ssthresh=" << ssthresh;
    return out.str();
}

std::string TCPTahoeRenoFamilyStateVariables::detailedInfo() const
{
    std::stringstream out;
    out << TCPBaseAlgStateVariables::detailedInfo();
    out << "ssthresh=" << ssthresh << "\n";
    return out.str();
}

//---

TCPTahoeRenoFamily::TCPTahoeRenoFamily() : TCPBaseAlg(),
        state((TCPTahoeRenoFamilyStateVariables *&)TCPAlgorithm::state)
{
}

void TCPTahoeRenoFamily::updateCwnd(){
    state->snd_cwnd = state->snd_cwnd/2;
    if (state->snd_cwnd < state->snd_mss)
    {
        state->snd_cwnd = state->snd_mss;
        state->ssthresh = 2 * state->snd_mss;
    }
    else if (state->snd_cwnd < 2*state->snd_mss)
        state->ssthresh = 2 * state->snd_mss;
    else
        state->ssthresh = state->snd_cwnd;

    if (cwndVector)
        cwndVector->record(state->snd_cwnd);
    if (ssthreshVector)
        ssthreshVector->record(state->ssthresh);
    state->snd_cwr_rtt = SIMTIME_DBL(simTime()) + state->rtt;
}

void TCPTahoeRenoFamily::sendErn()
{
    if(state->snd_cwnd >= state->ssthresh)
    {
        state->snd_ers = true;
        state->snd_ern = true;
        state->cwnd = state->snd_cwnd;
    }
    else
    {
        state->snd_ers = false;
        state->snd_ern = false;
    }
    state->snd_ern_rtt = SIMTIME_DBL(simTime()) + state->rtt;
}
